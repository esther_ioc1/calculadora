package ioc.xtec.cat.calculadora;
/**
 * Clase que proporciona operacions artimeticas basicas.
 * @author Esther Pont Martori
 * @version 1.0.0
 */
public class Operacions {
    /**
     * Metode per realitzar la suma de dos números.
     * @param a El primer número
     * @param b El segon número
     * @return La suma dels dos números.
     */
    public static double sumar(double a, double b) {
        return a + b;
    }
    /**
     * Metode per realitzar la resta de dos numeros.
     * @param a El primer numero
     * @param b El segon numero
     * @return La resta dels dos numeros.
     */
    public static double restar(double a, double b) {
        return a - b;
    }
    /**
     * Metode per realitzar la multiplicació de dos numeros.
     * @param a El primer numero
     * @param b El segon numero
     * @return El resultat de la multiplicació.
     */
      public static double multiplicar(double a, double b) {
        return a * b;
    }
    /**
     * Metode per realitzar la divisio de dos numeros.
     * @param a El divident
     * @param b El divisor
     * @return El resultat de la divisio.
     * @throws ArithmeticException Si el divisor es cero
     */
    public static double dividir(double a, double b) throws ArithmeticException {
        if (b == 0) {
            throw new ArithmeticException("El divisor no pot ser zero");
        }
        return a / b;
    }
}
